﻿using CATApi.Configurations;
using CATApi.Persistance.Repositories;
using CATApi.Persistance.Repositories.Interfaces;
using CATApi.Services;
using Microsoft.EntityFrameworkCore;

namespace CATApi.Persistance.DependencyInjection;

public static class ServiceExtension
{
    public static IServiceCollection AddDIServices(this IServiceCollection services, IConfiguration configuration)
    {
        services.AddDbContext<DataContext>(options =>
        {
            options.UseSqlite(configuration.GetConnectionString("CATApi"));
        });
        
        services.AddScoped<IUnitOfWork, UnitOfWork>();
        services.AddScoped<IFavoriteRepository, FavoriteRepository>();
        services.AddScoped<CatApiService>();
        services.AddScoped<FavoriteService>();
        services.AddOptions<TheCatApiConfiguration>().BindConfiguration(TheCatApiConfiguration.Key);

        return services;
    }
}