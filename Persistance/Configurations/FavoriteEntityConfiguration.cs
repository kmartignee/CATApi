﻿using CATApi.Persistance.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace CATApi.Persistance.Configurations;

public class FavoriteEntityConfiguration : IEntityTypeConfiguration<FavoriteEntity>
{
    public void Configure(EntityTypeBuilder<FavoriteEntity> builder)
    {
        builder.ToTable("CAT_FAVORITE_FAV");

        builder.HasKey(x => x.Id);

        builder.Property(x => x.Id).HasColumnName("FAV_ID");
        builder.Property(x => x.Url).HasColumnName("FAV_URL");
        builder.Property(x => x.Breed).HasColumnName("FAV_BREED");
    }
}