﻿namespace CATApi.Persistance.Entities;

public class FavoriteEntity
{
    public int Id { get; set; }
    public string Url { get; set; }
    public string? Breed { get; set; }
}