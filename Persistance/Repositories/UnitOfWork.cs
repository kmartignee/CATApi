﻿using CATApi.Persistance.Repositories.Interfaces;

namespace CATApi.Persistance.Repositories;

public class UnitOfWork : IUnitOfWork
{
    private readonly DataContext _dbContext;
    public IFavoriteRepository Favorites { get; }

    public UnitOfWork(DataContext dbContext,
        IFavoriteRepository favoriteRepository)
    {
        _dbContext = dbContext;
        Favorites = favoriteRepository;
    }

    public void Dispose()
    {
        Dispose(true);
        GC.SuppressFinalize(this);
    }

    protected virtual void Dispose(bool disposing)
    {
        if (disposing)
        {
            _dbContext.Dispose();
        }
    }
}