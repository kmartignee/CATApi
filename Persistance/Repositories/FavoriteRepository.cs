﻿using CATApi.Persistance.Entities;
using CATApi.Persistance.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace CATApi.Persistance.Repositories;

public class FavoriteRepository : GenericRepository<FavoriteEntity>, IFavoriteRepository
{
    private readonly DataContext _dbContext;
    public FavoriteRepository(DataContext dbContext) : base(dbContext)
    {
        _dbContext = dbContext;
    }
    
    public async Task<FavoriteEntity> GetFavoriteByUrlAsync(string url)
    {
        var entity = await _dbContext.Favorites.FirstOrDefaultAsync(f => f.Url == url);
        if (entity == null)
        {
            throw new Exception("Favorite not found");
        }
        
        return entity;
    }
}