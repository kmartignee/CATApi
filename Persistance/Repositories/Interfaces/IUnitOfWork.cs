﻿namespace CATApi.Persistance.Repositories.Interfaces;

public interface IUnitOfWork : IDisposable
{
    IFavoriteRepository Favorites { get; }
}