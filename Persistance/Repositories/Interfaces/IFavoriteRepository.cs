﻿using CATApi.Persistance.Entities;

namespace CATApi.Persistance.Repositories.Interfaces;

public interface IFavoriteRepository : IGenericRepository<FavoriteEntity>
{
    Task<FavoriteEntity> GetFavoriteByUrlAsync(string url);
}