﻿using System.Linq.Expressions;
using CATApi.Persistance.Repositories.Interfaces;

namespace CATApi.Persistance.Repositories;

public class GenericRepository<T> : IGenericRepository<T> where T : class
{
    protected readonly DataContext Context;

    public GenericRepository(DataContext context)
    {
        Context = context;
    }

    public async Task AddAsync(T entity)
    {
        Context.Set<T>().Add(entity);
        await Context.SaveChangesAsync();
    }

    public async Task AddRangeAsync(IEnumerable<T> entities)
    {
        Context.Set<T>().AddRange(entities);
        await Context.SaveChangesAsync();
    }

    public IEnumerable<T> Find(Expression<Func<T, bool>> expression)
    {
        return Context.Set<T>().Where(expression);
    }

    public IEnumerable<T> GetAll()
    {
        return Context.Set<T>().ToList();
    }

    public T? GetById(int id)
    {
        return Context.Set<T>().Find(id);
    }

    public async Task RemoveAsync(T entity)
    {
        Context.Set<T>().Remove(entity);
        await Context.SaveChangesAsync();
    }

    public async Task RemoveRangeAsync(IEnumerable<T> entities)
    {
        Context.Set<T>().RemoveRange(entities);
        await Context.SaveChangesAsync();
    }
}