﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace CATApi.Persistance.Migrations
{
    /// <inheritdoc />
    public partial class AddBreedColumn : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "FAV_BREED",
                table: "CAT_FAVORITE_FAV",
                type: "TEXT",
                nullable: false,
                defaultValue: "");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "FAV_BREED",
                table: "CAT_FAVORITE_FAV");
        }
    }
}
