﻿// <auto-generated />
using CATApi.Persistance;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

#nullable disable

namespace CATApi.Persistance.Migrations
{
    [DbContext(typeof(DataContext))]
    [Migration("20230919181017_Initial")]
    partial class Initial
    {
        /// <inheritdoc />
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder.HasAnnotation("ProductVersion", "7.0.11");

            modelBuilder.Entity("CATApi.Persistance.Entities.FavoriteEntity", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("INTEGER")
                        .HasColumnName("FAV_ID");

                    b.Property<string>("Url")
                        .IsRequired()
                        .HasColumnType("TEXT")
                        .HasColumnName("FAV_URL");

                    b.HasKey("Id");

                    b.ToTable("CAT_FAVORITE_FAV", (string)null);
                });
#pragma warning restore 612, 618
        }
    }
}
