﻿using CATApi.Persistance.Configurations;
using CATApi.Persistance.Entities;
using Microsoft.EntityFrameworkCore;

namespace CATApi.Persistance;

public class DataContext : DbContext
{
    public DataContext(DbContextOptions<DataContext> contextOptions) : base(contextOptions)
    {
        
    }
    
    public DbSet<FavoriteEntity> Favorites { get; set; }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.ApplyConfiguration(new FavoriteEntityConfiguration());
    }
}