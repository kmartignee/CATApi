﻿namespace CATApi.Configurations;

public class TheCatApiConfiguration
{
    public static string Key { get; set; } = nameof(TheCatApiConfiguration);
    public string BaseUrl { get; set; }
    public string ApiKey { get; set; }
}