﻿namespace CATApi.Model;

public class Favorite
{
    public int Id { get; set; }
    public string Url { get; set; }
    public string? Breed { get; set; }
}