﻿using System.Text.Json.Serialization;

namespace CATApi.Model;

public class Breed
{
    [JsonPropertyName("id")]
    public string Id { get; set; }
    
    [JsonPropertyName("name")]
    public string? Name { get; set; }
    
    [JsonPropertyName("origin")]
    public string Origin { get; set; }
    
    [JsonPropertyName("description")]
    public string Description { get; set; }
}