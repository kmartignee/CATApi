﻿using System.Text.Json;
using Ardalis.GuardClauses;
using CATApi.Configurations;
using CATApi.Model;
using Microsoft.Extensions.Options;

namespace CATApi.Services;

public class CatApiService
{
    private readonly TheCatApiConfiguration _theCatApiConfiguration;
    
    public CatApiService(IOptions<TheCatApiConfiguration> theCatApiConfiguration)
    {
        _theCatApiConfiguration = Guard.Against.Null(theCatApiConfiguration, nameof(theCatApiConfiguration)).Value;
    }
    
    // Récupère un chat aléatoire à partir de l'API
    public async Task<Cat> GetCatAsync()
    {
        var client = new HttpClient();
        var stream = await client.GetAsync($"{_theCatApiConfiguration.BaseUrl}/images/search?" +
                                           $"api_key={_theCatApiConfiguration.ApiKey}" +
                                           $"&has_breeds=1");
        var cats = JsonSerializer.Deserialize<List<Cat>>(await stream.Content.ReadAsStringAsync()) ?? new List<Cat>();
        return cats.FirstOrDefault() ?? new Cat();
    }
    
    // Récupère la liste des races de chats à partir de l'API
    public async Task<List<Breed>> GetBreedsAsync()
    {
        var client = new HttpClient();
        var stream = await client.GetAsync($"{_theCatApiConfiguration.BaseUrl}/breeds");
        return JsonSerializer.Deserialize<List<Breed>>(await stream.Content.ReadAsStringAsync()) ?? new List<Breed>();
    }
    
    // Récupère une liste de chats d'une race spécifique à partir de l'API
    public async Task<List<Cat>> GetCatsAsync(string breedId)
    {
        var client = new HttpClient();
        var stream = await client.GetAsync($"{_theCatApiConfiguration.BaseUrl}/images/search?" +
                                           $"breed_ids={breedId}" +
                                           $"&api_key={_theCatApiConfiguration.ApiKey}" +
                                           $"&limit=5");
        return JsonSerializer.Deserialize<List<Cat>>(await stream.Content.ReadAsStringAsync()) ?? new List<Cat>();
    }
}