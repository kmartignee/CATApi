﻿using Ardalis.GuardClauses;
using CATApi.Model;
using CATApi.Persistance.Entities;
using CATApi.Persistance.Repositories.Interfaces;

namespace CATApi.Services;

public class FavoriteService
{
    private readonly IUnitOfWork _unitOfWork;

    public FavoriteService(IUnitOfWork unitOfWork)
    {
        _unitOfWork = Guard.Against.Null(unitOfWork, nameof(unitOfWork));
    }

    
    // Récupère la liste des favoris
    public List<Favorite> GetFavorites()
    {
        var entities = _unitOfWork.Favorites.GetAll();
        return entities.Select(entity => new Favorite
        {
            Id = entity.Id,
            Url = entity.Url,
            Breed = entity.Breed
        }).ToList();
    }
    
    
    // Ajoute un favori de manière asynchrone
    public async Task AddFavoriteAsync(Favorite favorite)
    {
        var entity = new FavoriteEntity
        {
            Url = favorite.Url,
            Breed = favorite.Breed
        };

        await _unitOfWork.Favorites.AddAsync(entity);
    }
    
    // Supprime un favori par ID de manière asynchrone
    public async Task RemoveFavoriteByIdAsync(int id)
    {
        var entity = _unitOfWork.Favorites.GetById(id);
        if (entity == null)
        {
            throw new NotFoundException($"Id: {id}", "Favorite");
        }
        await _unitOfWork.Favorites.RemoveAsync(entity);
    }
    
    // Supprime un favori par URL de manière asynchrone
    public async Task RemoveFavoriteByUrlAsync(string url)
    {
        var entity = await _unitOfWork.Favorites.GetFavoriteByUrlAsync(url);
        if (entity == null)
        {
            throw new NotFoundException($"Url: {url}", "Favorite");
        }
        await _unitOfWork.Favorites.RemoveAsync(entity);
    }
}